
//TODO provide multiple covariance functions
function squaredExponential(x1, x2, hyperParams) {
    var h = hyperParams[0];
    var l = hyperParams[1];
    var d = x1 - x2;
    var norm_d = Math.pow(d, 2);
    return h * Math.exp(-(norm_d / (2 * l * l)));
};

function covarianceMatrix(X, covariance, hyperParams) {
    return $M(X.map(function(x1) {
        return X.map(function(x2) {
            return covariance(x1, x2, hyperParams);
        }).elements;
    }).elements);
};

function gpMean(s, X, Y, C, covariance, hyperParams) {
    var k = $M(X.map(function(x) { return covariance(s, x, hyperParams); }));
    return k.transpose().x(C.inv().x(Y)).e(1);
};

function gpVariance(s, X, Y, C, covariance, hyperParams) {
    var k = $M(X.map(function(x) { return covariance(s, x, hyperParams); }));
    return covariance(s, s, hyperParams) - k.transpose().x(C.inv().x(k)).e(1, 1);
};

var X = [1, 2];//TODO allow X and Y to be empty
var Y = [1, 2];
var samples = [[1,1], [2,2]];//TODO zip based on X and Y

function createDataset() {
    var hyperParams = [ Number($("#sigma_h").val()), Number($("#sigma_l").val()) ];
    var C = covarianceMatrix($V(X), squaredExponential, hyperParams);
    var MU = [];
    var SIGMA_UPPER = [];
    var SIGMA_LOWER = [];
    for (var i = -10; i <= 10; i += 0.1) {
        var mu = gpMean(i, $V(X), $V(Y), C, squaredExponential, hyperParams);
        MU.push([i, mu]);
        SIGMA_UPPER.push([i, mu + gpVariance(i, $V(X), $V(Y), C, squaredExponential, hyperParams)]);
        SIGMA_LOWER.push([i, mu - gpVariance(i, $V(X), $V(Y), C, squaredExponential, hyperParams)]);
    }

    return [
        { data: MU, lines: { show: true }, color: "rgb(255, 50, 50)" },
        { data: SIGMA_UPPER, id: "sigma_upper", lines: { show: true, lineWidth: 0, fill: false }, color: "rgb(255, 50, 50)" },
        { data: SIGMA_LOWER, lines: { show: true, lineWidth: 0, fill: 0.4 }, color: "rgb(255, 50, 50)", fillBetween: "sigma_upper"},
        { data: samples, lines: { show: false }, points: { show: true }, color: "rgb(255, 50, 50)" }
        ];
};

function plotDataset(id, dataset) {
    $.plot($(id), dataset, { grid: { clickable: true } });
}

var init = function(id) {
    plotDataset(id, createDataset());
    $(id).bind("plotclick", function(event, pos, item) {
        X.push(pos.x);
        Y.push(pos.y);
        samples.push([pos.x, pos.y]);
        plotDataset(id, createDataset());
    });
    $("#sigma_h").blur(function() { plotDataset(id, createDataset()); });
    $("#sigma_l").blur(function() { plotDataset(id, createDataset()); });
}
